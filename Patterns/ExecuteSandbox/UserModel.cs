﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExecuteSandbox {
	public class UserModel : IComparable {
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public int Age { get; set; }
		public string Status { get; set; }
		public string Gender { get; set; }

		public int CompareTo(object obj) {
			UserModel temp = obj as UserModel;
			if (temp != null) {
				return String.Compare(this.FirstName, temp.FirstName);
			} else {
				throw new ArgumentException("Invalid parameter");
			}
		}

		public static IComparer<UserModel> SortByAge { get { return new CompareByAge(); } }
	}
}
