﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExecuteSandbox.Enumerable {
	public class Garage: IEnumerable {
		private List<UserModel> userList = new List<UserModel>(4);

		public Garage() {
			userList.Add(new UserModel { FirstName = "Joe" });
			userList.Add(new UserModel { FirstName = "Chloe" });
		}


		public IEnumerator GetEnumerator() {
			return userList.GetEnumerator();
		}
	}
}
