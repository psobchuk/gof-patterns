﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ExecuteSandbox {
	public class CompareByAge : IComparer<UserModel> {
		public int Compare(UserModel x, UserModel y) {
			if (x != null && y != null) {
				return x.Age.CompareTo(y.Age);
			} else {
				throw new ArgumentException("Ivalid parameters");
			}
		}
	}
}
