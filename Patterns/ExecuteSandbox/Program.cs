﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using ExecuteSandbox.Enumerable;

namespace ExecuteSandbox {
	
	
	class Program {
		public static void ComparableSample() {
			List<UserModel> users = new List<UserModel> {
				new UserModel {
					FirstName = "Audrey",
					LastName = "234",
					Age = 10,
					Status = "OK",
						Gender = "m"
					},
				new UserModel {
					FirstName = "Zack",
					LastName = "234",
					Age = 15,
					Status = "OK",
					Gender = "m"
				},
				new UserModel {
					FirstName = "Ben",
					LastName = "234",
					Age = 9,
					Status = "OK",
					Gender = "m"
				}
			};

			foreach (UserModel user in users) {
				Console.WriteLine(user.FirstName + " " + user.Age);

			}
			Console.WriteLine();

			users.Sort(UserModel.SortByAge);

			foreach (UserModel user in users) {
				Console.WriteLine(user.FirstName + " " + user.Age);
			}
			Console.WriteLine();

			users.Sort();

			foreach (UserModel user in users) {
				Console.WriteLine(user.FirstName + " " + user.Age);
			}
			Console.WriteLine();

			Console.ReadLine();
		}

		public static void EnumerableSample() {
			Garage garage = new Garage();
			foreach (UserModel user in garage) {
				Console.WriteLine("username: " + user.FirstName);
			}
		}

		static void Main(string[] args) {

			EnumerableSample();
			Console.ReadLine();
		}
	}
}
