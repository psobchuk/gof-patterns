﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Facade.Services;

namespace Facade {
	public class ServiceFacade {
		private List<ISearchService> searchServices;
		
		public ServiceFacade() {
			searchServices = new List<ISearchService> {
				new GoogleService(),
				new YahooService(),
				new BingService()
			};
		}

		public List<string> GetServicesData(string query) {
			List<string> data = new List<string>();
			foreach (ISearchService service in searchServices) {
				data.AddRange(service.GetData(query));
			}

			return data;
		}

	}
}
