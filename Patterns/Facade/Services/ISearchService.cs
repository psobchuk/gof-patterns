﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Facade.Services {
	interface ISearchService {
		List<string> GetData(string query);
	}
}
