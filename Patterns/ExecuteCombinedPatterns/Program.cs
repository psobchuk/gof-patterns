﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using McDonaldsSample;
using McDonaldsSample.McDonaldCombos;

namespace ExecuteCombinedPatterns {
    class Program {
        static void Main(string[] args) {
            Order order = new Order();
            order.Box = BoxType.None;
            order.BreweryName = "sprite";
            order.CondimentName = "ketchup";
            order.MealName = "hamburger";

            Cassier cassier = new Cassier();
            cassier.ReceiveOrder(order);
            Combo product = cassier.ReturnProduct();
        }
    }
}
