﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Facade;
using AbstractFactoryMethod.Factories;
using AbstractFactoryMethod.Items;
using Singleton;
using Builder;
using Builder.Builders;
using Prototype;
using Bridge;
using Bridge.Boxes;
using Bridge.ProductBuilders;

namespace ExecutePatterns {
	class Program {
        static void FacadeSample() {
            //Facade example
            ServiceFacade facade = new ServiceFacade();
            List<string> data = facade.GetServicesData("Ukraine news");
        }

        static void FactoryAndMethodSample() {
            //Factory and factory method example
            BaseReportElementFactory factory = new TrendReportElementFactory();
            ReportElement chartElement = factory.DeliverReportElement(ReportElementType.ChartReportElement);
            ReportElement tableElement = factory.DeliverReportElement(ReportElementType.TableReportElement);
        }

        static void SingletonSample() {
            //Singleton example
            CacheManager.Cache.Set(new object(), "12345");
            Object obj = CacheManager.Cache.Get("12345");
        }

        static void BuilderSample() {
            //Builder example
            VehicleMaker maker = new VehicleMaker(new MotorcycleBuilder());
            Vehicle vehicle = maker.GetVehicle();
            Console.WriteLine(vehicle.ToString());
        }

        static void PrototypeSample() {
            //WebPageScraper scraper = new WebPageScraper("http://www.google.com");
            //scraper.PrintPageData();
            //WebPageScraper scraper2 = scraper.Clone() as WebPageScraper;
            //scraper2.PrintPageData();

            var puzzle = new LogicPuzzle();
            puzzle.SwitchBlueSwitch();
            puzzle.ToggleBigToggle();
            puzzle.SwitchBlueSwitch();
            puzzle.SwitchRedSwitch();

            puzzle.PrintState();
            Console.WriteLine();
            var puzzle2 = puzzle.Clone() as LogicPuzzle;
            puzzle2.PrintState();
        }

        static void BridgeSample() {
            ProductMaker maker = new ProductMaker(new HamburgerBuilder(), new ChildernBox());
            object result = maker.DeliverProduct();
        }

        static void Main(string[] args) {
            PrototypeSample();

			Console.ReadLine();
		}
	}
}
