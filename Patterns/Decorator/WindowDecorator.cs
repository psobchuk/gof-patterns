﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Decorator {
    public abstract class WindowDecorator: Window {
        protected Window window;

        public WindowDecorator(Window window) {
            this.window = window;
        }

        public override void OpenWindow() {
            window.OpenWindow();
        }
    }
}
