﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Decorator {
   public class MicrosoftWindow: Window {
       public override void OpenWindow() {
           Console.WriteLine("Opened window...");
       }
   }
}
