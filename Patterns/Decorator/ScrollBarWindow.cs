﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Decorator {
    public class ScrollBarWindow: WindowDecorator {
        public string ScrollBar { get; set; }
        public ScrollBarWindow(Window window) : base(window) { }

        public void AddScrollbar() {
            Console.WriteLine("Scrollbar added...");
        }
    }
}
