﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Decorator
{
    public abstract class Window
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public abstract void OpenWindow();
    }
}
