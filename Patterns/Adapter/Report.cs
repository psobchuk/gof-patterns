﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adapter
{
    public class Report
    {
        public ReportTarget Target;

        public void SetTarget(string type) {
            switch (type) {
                case "sent":
                    Target = new SentimentReportTarget();
                    break;
                case "trend":
                    //use adapter to convert incompatible interfaces
                    Target = new TrendReportTarget(new Adapter.ThirdParty.TrendTarget());
                    break;
            }
        }
    }
}
