﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Adapter.ThirdParty;

namespace Adapter {
    class TrendReportTarget: ReportTarget {
        private TrendTarget target;

        public TrendReportTarget(TrendTarget target) {
            this.target = target;
        }

        public override ReportTarget DeliverReportTarget() {
            target.DeliverTarget();
            return null;
        }
    }
}
