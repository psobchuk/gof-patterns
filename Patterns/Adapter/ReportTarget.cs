﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Adapter {
    public abstract class ReportTarget {
        public string SomeProperty { get; set; }
        public int AnotherProperty { get; set; }
        public abstract ReportTarget DeliverReportTarget();
    }
}
