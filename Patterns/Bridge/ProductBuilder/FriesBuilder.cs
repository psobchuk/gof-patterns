﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridge.ProductBuilders {
    public class FriesBuilder: ProductBuilder {
        private override void CreateName() {
            product.Name = "Fries";
        }

        private override void SetCost() {
            product.Cost = 10.50;
        }

        private override void SetIngredients() {
            product.Ingredients = new Dictionary<string, int>();
            product.Ingredients.Add("Potato", 300);
            product.Ingredients.Add("Salt", 100);
        }
    }
}
