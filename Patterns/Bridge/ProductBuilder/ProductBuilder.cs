﻿using Bridge.Boxes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridge.ProductBuilders {
    public abstract class ProductBuilder {
        protected Product product;
        private abstract void CreateName();
        private abstract void SetCost();
        private abstract void SetIngredients();

        public Product CreateProduct() {
            product = new Product();
            CreateName();
            CreateProduct();
            SetIngredients();

            return product;
        }

        //creating a bridge here
        public string CreateProductInBox(IBox box) {
            CreateProduct();
            return box.PackToBox(product);
        }
    }
}
