﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridge.ProductBuilders {
   public class HamburgerBuilder: ProductBuilder {
        private override void CreateName() {
            product.Name = "Hamburger";
        }

        private override void SetCost() {
            product.Cost = 20.50;
        }

        private override void SetIngredients() {
            product.Ingredients = new Dictionary<string, int>();
            product.Ingredients.Add("Ham", 100);
            product.Ingredients.Add("Pickles", 50);
            product.Ingredients.Add("Onion", 100);
        }
    }
}
