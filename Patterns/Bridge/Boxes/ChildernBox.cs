﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridge.Boxes {
    public class ChildernBox: IBox {
        public string PackToBox(Product product) {
            return "Wrapped product to a children box";
        }
    }
}
