﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bridge
{
    public class Product {
        public string Name { get; set; }
        public double Cost { get; set; }
        public Dictionary<string, int> Ingredients { get; set; }

        public override string ToString() {
            return string.Format("Name: {0}, Cost: {1}", Name, Cost);
        }
    }
}
