﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bridge.ProductBuilders;
using Bridge.Boxes;

namespace Bridge {
    public class ProductMaker {
        ProductBuilder builder;
        IBox box;
        public ProductMaker(ProductBuilder builder, IBox box) {
            this.builder = builder;
            this.box = box;
        }

        public object DeliverProduct() {
            if (box == null) {
                return builder.CreateProduct();
            } else {
                return builder.CreateProductInBox(box);
            }
        }
    }
}
