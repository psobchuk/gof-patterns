﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builder.Builders {
	public abstract class VehicleBuilder {
		protected Vehicle vehicle;
		
		protected abstract void CreateDoors();
		protected abstract void CreateWheels();
		protected abstract void SetMaxSpeed();
		protected abstract void SetName();

		public void CreateVehicle() {
			vehicle = new Vehicle();
			CreateDoors();
			CreateWheels();
			SetMaxSpeed();
			SetName();
		}

		public Vehicle GetVehicle() {
			return vehicle;
		}

	}
}
