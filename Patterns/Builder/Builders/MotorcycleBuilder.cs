﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builder.Builders {
	public class MotorcycleBuilder: VehicleBuilder {
		protected override void CreateDoors() {
			vehicle.DoorCount = 0;
		}

		protected override void CreateWheels() {
			vehicle.WheelCount = 2;
		}

		protected override void SetMaxSpeed() {
			vehicle.MaxSpeed = 300;
		}

		protected override void SetName() {
			vehicle.Name = "Motorcycle";
		}
	}
}
