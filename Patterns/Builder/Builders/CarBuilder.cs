﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builder.Builders {
	public class CarBuilder: VehicleBuilder {
		protected override void CreateDoors() {
			vehicle.DoorCount = 4;
		}

		protected override void CreateWheels() {
			vehicle.WheelCount = 4;
		}

		protected override void SetMaxSpeed() {
			vehicle.MaxSpeed = 200;
		}

		protected override void SetName() {
			vehicle.Name = "Car";
		}
	}
}
