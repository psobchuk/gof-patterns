﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Builder.Builders;

namespace Builder {
	public class VehicleMaker {
		private VehicleBuilder builder;

		public VehicleMaker(VehicleBuilder builder) {
			this.builder = builder;
		}
		
		public Vehicle GetVehicle() {
			builder.CreateVehicle();
			return builder.GetVehicle();
		}
	}
}
