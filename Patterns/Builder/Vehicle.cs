﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Builder {
	public class Vehicle {
		public int DoorCount { get; set; }
		public int WheelCount { get; set; }
		public int MaxSpeed { get; set; }
		public string Name { get; set; }

		public override string ToString() {
			return "Door count: " + DoorCount + " WheelCount: " + WheelCount + " MaxSpeed: " + MaxSpeed + " Name: " + Name;
		}
	}
}
