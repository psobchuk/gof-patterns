﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace McDonaldsSample.Products {
    public abstract class Product {
        public string Name { get; set; }
        public string Cost { get; set; }
    }
}
