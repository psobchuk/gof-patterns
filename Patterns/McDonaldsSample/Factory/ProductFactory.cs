﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using McDonaldsSample.Products;

namespace McDonaldsSample.Factory {
   internal abstract class ProductFactory {
       internal abstract Product CreateProduct();
    }
}
