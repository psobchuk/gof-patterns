﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using McDonaldsSample.McDonaldCombos;

namespace McDonaldsSample.Factory {
    //part of builder pattern
    internal class Kitchen {
        private Combo comboProduct;
        private ProductFactory factory;
        internal void CreateProduct(Order order) {
            comboProduct = new Combo();
            CreateMeal(order.MealName);
            CreateBrewery(order.BreweryName);
            CreateCondiment(order.CondimentName);
        }

        internal Combo GetCombo() {
            return comboProduct;
        }

        private void CreateMeal(string mealName) {
            factory = new MealFactory();
            comboProduct.Meal = factory.CreateProduct();
        }

        private void CreateBrewery(string breweryName) {
            factory = new BreweryFactory();
            comboProduct.Brewery = factory.CreateProduct();
        }

        private void CreateCondiment(string condimentName) {
            factory = new CondimentFactory();
            comboProduct.Condiment = factory.CreateProduct();
        }
    }
}
