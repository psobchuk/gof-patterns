﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using McDonaldsSample.Products;

namespace McDonaldsSample.Factory {
    internal class BreweryFactory: ProductFactory {
        internal override Product CreateProduct() {
            return new Brewery();
        }
    }
}
