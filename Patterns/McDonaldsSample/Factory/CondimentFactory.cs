﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using McDonaldsSample.Products;

namespace McDonaldsSample.Factory {
    internal class CondimentFactory: ProductFactory {
        internal override Product CreateProduct() {
            return new Condiment();
        }
    }
}
