﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using McDonaldsSample.Products;
namespace McDonaldsSample.McDonaldCombos {
    public class Combo {
        public Product Meal { get; set; }
        public Product Brewery { get; set; }
        public Product Condiment { get; set; }
    }
}
