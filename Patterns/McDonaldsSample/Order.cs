﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using McDonaldsSample.Products;

namespace McDonaldsSample {
    public class Order {
        public string MealName { get; set; }
        public string BreweryName { get; set; }
        public string CondimentName { get; set; }
        public BoxType Box { get; set; }
    }
}
