﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using McDonaldsSample.McDonaldCombos;
using McDonaldsSample.Factory;

namespace McDonaldsSample {
    //facade
    public class Cassier {
        private Kitchen kitchen;
        public void ReceiveOrder(Order order) {
            kitchen.CreateProduct(order);
        }
        public Combo ReturnProduct() {
            return kitchen.GetCombo();
        }
    }
}
