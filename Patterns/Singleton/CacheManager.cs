﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Singleton {
	public class CacheManager {
		private static volatile CacheManager cacheManager;
		private static object locker = new Object();
		private CacheManager() {}

		public static CacheManager Cache {
			get {
				if (cacheManager == null) {
					lock (locker) {
						if (cacheManager == null) {
							cacheManager = new CacheManager();
						}
					}
				}
				return cacheManager;
			}
		}

		public void Set(Object element, string key) { }
		public Object Get(string key) { return new Object(); }
	}
}
