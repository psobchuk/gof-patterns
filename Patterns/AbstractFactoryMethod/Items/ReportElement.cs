﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractFactoryMethod.Items {
	public abstract class ReportElement {
		//some properties of report element
	}

	public enum ReportElementType {
		TableReportElement, ChartReportElement
	}
}
