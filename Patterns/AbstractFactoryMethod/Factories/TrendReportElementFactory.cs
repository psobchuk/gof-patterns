﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AbstractFactoryMethod.Items;

namespace AbstractFactoryMethod.Factories {
	public class TrendReportElementFactory: BaseReportElementFactory {
		public override ReportElement CreateReportElement(ReportElementType elementType) {
			switch (elementType) {
				case ReportElementType.ChartReportElement:
					return new ChartReportElement();
				case ReportElementType.TableReportElement:
					return new TableReportElement();
				default:
					return null;
			}
		}
	}
}
