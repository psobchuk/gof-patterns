﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AbstractFactoryMethod.Items;

namespace AbstractFactoryMethod.Factories {
	//abstract factory
	public abstract class BaseReportElementFactory {
		public ReportElement DeliverReportElement(ReportElementType elementType) {
			ReportElement element = CreateReportElement(elementType);
			ApplyReportElementSettings(element);
			ApplyReportElementStyles(element);
			return element;
		}

		//factory method
		public abstract ReportElement CreateReportElement(ReportElementType elementType);

		private void ApplyReportElementSettings(ReportElement element) {
			throw new NotImplementedException();
		}

		private void ApplyReportElementStyles(ReportElement element) {
			throw new NotImplementedException();
		}

	}
}
